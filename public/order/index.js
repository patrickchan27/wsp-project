
let currentType = 0;
// ************************************************
// Shopping Cart API
// ************************************************

var shoppingCart = (function () {
  // =============================
  // Private methods and propeties
  // =============================
  cart = [];

  // Constructor
  function Item(name, price, count, items) {
    this.name = name;
    this.price = price;
    this.count = count;
    this.items = items

  }

  // Save cart
  function saveCart() {
    sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
  }

  // Load cart
  function loadCart() {
    cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
  }
  if (sessionStorage.getItem("shoppingCart") != null) {
    loadCart();
  }


  // =============================
  // Public methods and propeties
  // =============================
  var obj = {};

  // Add to cart

  obj.addItemToCart = function (name, price, count, items) {
    for (var item in cart) {
      if ((!items || items.length == 0) && cart[item].name === name) {
        cart[item].count++;
        saveCart();
        return;
      }
    }
    var item = new Item(name, price, count, items);
    cart.push(item);
    saveCart();

  }
  // Set count from item
  obj.setCountForItem = function (name, count) {
    for (var i in cart) {
      if (cart[i].name === name) {
        cart[i].count = count;
        break;
      }
    }
  };
  // Remove item from cart
  obj.removeItemFromCart = function (name) {
    for (var item in cart) {
      if (cart[item].name === name) {
        cart[item].count--;
        if (cart[item].count === 0) {
          cart.splice(item, 1);
        }
        break;
      }
    }
    saveCart();
  }

  // Remove all items from cart
  obj.removeItemFromCartAll = function (name) {
    for (var item in cart) {
      if (cart[item].name === name) {
        cart.splice(item, 1);
        break;
      }
    }
    saveCart();
  }

  // Clear cart
  obj.clearCart = function () {
    cart = [];
    saveCart();
  }

  // Count cart 
  obj.totalCount = function () {
    var totalCount = 0;
    for (var item in cart) {
      totalCount += cart[item].count;
    }
    return totalCount;
  }

  // Total cart
  obj.totalCart = function () {
    var totalCart = 0;
    for (var item in cart) {
      totalCart += cart[item].price * cart[item].count;
    }
    return Number(totalCart.toFixed(2));
  }

  // List cart
  obj.listCart = function () {
    var cartCopy = [];
    for (i in cart) {
      item = cart[i];
      let itemCopy = {};

      for (p in item) {
        itemCopy[p] = item[p];
        let optionCopy = []
        for (const j in item.items) {
          optionCopy[j] = item.items[j].name
        }



      }
      itemCopy.total = Number(item.price * item.count).toFixed(2);
      cartCopy.push(itemCopy)
    }
    return cartCopy;
  }

  // cart : Array
  // Item : Object/Class
  // addItemToCart : Function
  // removeItemFromCart : Function
  // removeItemFromCartAll : Function
  // clearCart : Function
  // countCart : Function
  // totalCart : Function
  // listCart : Function
  // saveCart : Function
  // loadCart : Function
  return obj;
})();



// *****************************************
// Triggers / Events
// ***************************************** 
// Add item
function addfoodlistener() {
  $('.add-to-cart').click(function (event) {
    event.preventDefault();


    var dishId = $(this).data('id');
    let options = reUseMenu.menu[currentType].dishes.find(e => e.id == dishId).options
    if (options && options.length > 0) {
      $('#optionmodal').modal('show')
      console.log(reUseMenu.menu[currentType])
      let optionhtml = ''
      for (let y = 0; y < options.length; y++) {
        const option = options[y];
        optionhtml += `<h4>${option.title} - ${option.necessity}</h4>`
        let inputType = '';
        if (option.maxItems == 1) {
          inputType = 'radio';
        } else {
          inputType = 'checkbox';
        }
        for (let i = 0; i < option.items.length; i++) {
          const item = option.items[i];
          optionhtml += `<div class="custom-control custom-${inputType}">
              <input class="custom-control-input" type="${inputType}" name="item${y}" id="item${y}-${i}" value="${i}"${option.necessity === 'Required' && i === 0 ? ' required' : ''}>
              <label class="custom-control-label" for="item${y}-${i}">
                ${item.name}
                </label>
                <span class="float-right">+$${item.price}</span>
            </div>`
        }
      }

      $('#optionPopUp').html(optionhtml)
      $('#formAddToCart').off();
      $('#formAddToCart').submit((event) => {
        event.preventDefault();
        var name = $(this).data('name');
        var price = Number($(this).data('price'));
        let selectedItem = [];

        for (let y = 0; y < options.length; y++) {
          const option = options[y];
          const itemsHtml = document.querySelectorAll(`input[name="item${y}"]`)

          for (let i = 0; i < option.items.length; i++) {
            const item = option.items[i];
            if (itemsHtml[i].checked) {
              price += parseFloat(item.price)
              selectedItem.push(item)
            }
          }


        }



        shoppingCart.addItemToCart(name, price, 1, selectedItem);
        displayCart();
        $('#optionmodal').modal('hide')
      })
    }
    else {
      var name = $(this).data('name');
      var price = Number($(this).data('price'));
      shoppingCart.addItemToCart(name, price, 1);
      displayCart();
    }
  });
}
// Clear items
$('.clear-cart').click(function () {
  shoppingCart.clearCart();
  displayCart();
});


function displayCart() {
  var cartArray = shoppingCart.listCart();
  var output = "";
  for (var i in cartArray) {
    output += "<tr>"
      + "<td>" + cartArray[i].name + "<br>"
    if (cartArray[i].items) {
      for (const item of cartArray[i].items) {
        output += " - " + item.name + "<br>"
      }
    }

    output += "</td>" //+ "<td>(" + cartArray[i].price + ")</td>"
      + "<td><div class='input-group'><button class='minus-item input-group-addon btn btn-primary' data-name='" + cartArray[i].name + "'>-</button>"
      + "<input type='number' class='item-count form-control' data-name='" + cartArray[i].name + "' value='" + cartArray[i].count + "'>"
      + "<button class='plus-item btn btn-primary input-group-addon' data-name='" + cartArray[i].name + "'>+</button></div></td>"
      //+ "<td><button class='delete-item btn btn-danger' data-name='" + cartArray[i].name + "'>X</button></td>"
      + " = "
      + "<td>" + cartArray[i].total + "</td>"
      + "</tr>";
  }
  $('.show-cart').html(output);
  $('.total-cart').html(shoppingCart.totalCart());
  $('.total-count').html(shoppingCart.totalCount());
}

// Delete item button

$('.show-cart').on("click", ".delete-item", function (event) {
  const name = $(this).data('name')
  shoppingCart.removeItemFromCartAll(name);
  displayCart();
})


// -1
$('.show-cart').on("click", ".minus-item", function (event) {
  const name = $(this).data('name')
  shoppingCart.removeItemFromCart(name);
  displayCart();
})
// +1
$('.show-cart').on("click", ".plus-item", function (event) {
  const name = $(this).data('name')
  shoppingCart.addItemToCart(name);
  displayCart();
})

// Item count input
$('.show-cart').on("change", ".item-count", function (event) {
  var name = $(this).data('name');
  var count = Number($(this).val());
  shoppingCart.setCountForItem(name, count);
  displayCart();
});

displayCart();
let reusemenu;

async function getFoodType() {
  const res = await fetch('/rest/menu');
  const obj = await res.json();
  reUseMenu = obj;
  const navClass = document.querySelector(".navbar-nav")
  let htmlStr = ''
  for (let i = 0; i < reUseMenu.menu.length; i++) {
    htmlStr += ' <a class="nav-item nav-link" href="javascript:loadFood(' + i + ');">' + reUseMenu.menu[i].name + '</a>';
  }

  navClass.innerHTML = htmlStr

  loadFood(0);
}
getFoodType();


function loadFood(index) {
  currentType = index;
  const menu = document.querySelector("#menu")
  let foodhtml = ''
  for (const dish of reUseMenu.menu[index].dishes) {
    foodhtml += `<div class="col-12 col-md-6 col-lg-3 mb-2">
     <div class="card border-0 shadow">
           <img class="card-img-top" src="/images/dishes/${dish.image}" alt="404 Not Found">
           <div class="card-body p-2">
                 <h5 class="card-title">${dish.dish}</h5>
                 <p class="card-text text-muted" style="font-size:14px">${dish.description}</p>
                 <p class="card-text">Price: $${dish.dishPrice}</p>
                 <a href="#" data-id="${dish.id}" data-name="${dish.dish}" data-price="${dish.dishPrice}"
                       class="add-to-cart btn btn-success">Add to cart</a>
           </div>
     </div>
</div>`

  }
  menu.innerHTML = foodhtml
  addfoodlistener();

}



document.querySelector("#orderButton").addEventListener('click', async function (e) {
  const res = await fetch('/rest/order', {
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    body: JSON.stringify({
      items: shoppingCart.listCart()
    })
  });
  console.log(shoppingCart.listCart());
  console.log(await res.json())
  shoppingCart.clearCart();
  displayCart();
})

shoppingCart.clearCart();
displayCart();

const urlParams = new URLSearchParams(window.location.search);
const tableID = parseInt(urlParams.get('table'));
document.querySelector("#tableNum").innerHTML = `<h5 class="my-auto">Table ${tableID}</h5>`;
const restName = urlParams.get('restName');
document.querySelector("#restName").innerHTML = restName;

async function getReceipt() {
  $('#tinfoModalbody').html(await receiptHtml())
}

async function receiptHtml() {
  const ordered = await (await fetch("../rest/ordered")).json();
  console.log(ordered);
  if (ordered.list && ordered.list.length > 0) {
    let total = 0;
    for (let i = 0; i < ordered.list.length; i++) {
      total += ordered.list[i].price * ordered.list[i].count;
      if (ordered.list[i].items) {
        for (let j = 0; j < ordered.list[i].items.length; j++) {
          total += Number(ordered.list[i].items[j].price);
        }
      }
    }
    return `
            ${loopItems(ordered.list)}
            <hr>
            <span class="text-muted">Total:</span>
            <h2 class="float-right">$${total}</h2>
            `;
  }
  else {
    return "You haven't ordered yet.";
  }
}

function loopItems(items) {
  let html = "";
  for (let i = 0; i < items.length; i++) {
    html += `
        <div>
            ${items[i].name} x ${items[i].count}
            <span class="float-right">
                $${items[i].total}
            </span>
            ${loopOptions(items[i].items)}
        </div>
        `;
  }
  return html;
}

function loopOptions(options) {
  if (options) {
    let html = "<ul>";
    for (let i = 0; i < options.length; i++) {
      html += `
            <li>
                ${options[i].name}
                <span class="float-right">
                    + $${options[i].price}
                </span>
            </li>
            `;
    }
    return html;
  }
  return "";
}