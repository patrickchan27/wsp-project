import { TableService } from "../services/TableService";
import * as express from "express";
import { Request, Response } from "express";



export class TableRouter
{

    constructor(private service: TableService)
    {}

    router()
    {
        const router = express.Router();
        router.get("/data", this.getTables);
        router.post("/", this.setTables);
        return router;
    }

    getTables = (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            res.json({
                result: true,
                tables: this.service.getTables(req.user["id"])
            });
        }
        else
        {
            res.json({
                result: false,
                message: "login required"
            });
        }
    }

    setTables = (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            this.service.setTables(req.user["id"], req.body.tables);
            res.json({
                result: Boolean(req.body.tables)
            });
        }
        else
        {
            res.json({
                result: false,
                message: "login required"
            });
        }
    }

}

