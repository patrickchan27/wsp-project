import * as express from "express";
import { Request, Response } from "express";
import * as passport from "passport";
import { loginFlow } from "../guards";
import { UserService } from "../services/UserService";
import { hashPassword } from "../hash";
import { sendMail } from "../mailer";


export class UserRouter {

    constructor(private service: UserService) {}

    router() {
        const router = express.Router();

        router.post("/retrieveInfo", this.retrieveInfo);

        router.post("/login", (...rest) => 
            passport.authenticate("local", loginFlow(...rest))(...rest)
        );

        router.get("/google", passport.authenticate("google", {
            scope: ["email", "profile"]
        }));

        router.get("/google/callback", (...rest) =>
            passport.authenticate("google", loginFlow(...rest))(...rest)
        );

        router.get("/logout", this.logout);

        router.post("/register", this.register);
        router.post("/change", this.change);

        router.get("/info", this.getUserInfo);

        return router;
    }

    private logout = (req: Request, res: Response) => {
        req.logOut();
        res.redirect("/");
    };

    private register = async (req: Request, res: Response) => {
        let {restName, email, username, password, cpassword} = req.body;
        if (restName && email && username && password && cpassword && password === cpassword)
        {
            res.json({
                result: this.service.register({
                    id: 0,
                    googleUser: false,
                    restName,
                    email,
                    username,
                    password: await hashPassword(password)
                })
            });
        }
        else
        {
            res.json({
                result: false
            });
        }
    }

    retrieveInfo = async (req: Request, res: Response) =>
    {
        let user = this.service.getUsers().find(e => e.email == req.body.email);
        if (user)
        {
            if (user.googleUser)
            {
                res.redirect("../login/login.html?error=Cannot retrieve info of Google account");
                return;
            }
            let password = "";
            let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            for (let i = 0; i < 20; i++) {
                password += characters.charAt(Math.floor(Math.random() * characters.length));
            }
            let result = await this.service.changeInfo(user.id, "", "", "", password, password)
            sendMail(user.email, "Retrieve Info", (result
                ? `
                <h1 style="margin-bottom: 0">Successfuly Retrieved Info</h1>
                <span style="color:red">Your password has been changed</span>
                <h3 style="margin-bottom: 0">User Info:</h3>
                Username: ${user.username}<br>Password: ${password}`
                : `Failed to retrieve info`
            ));
        }
        res.redirect("/");
    }

    getUserInfo = (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            let userInfo = this.service.getUserInfo(req.user["id"]);
            if (userInfo)
            {
                res.json({
                    result: true,
                    info: userInfo
                });
                return;
            }
        }
        res.json({
            result: false
        });
    }

    change = async (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            let {restName, email, username, password, cpassword} = req.body;
            await this.service.changeInfo(req.user["id"], restName, email, username, password, cpassword)
            res.redirect("/settings/settings.html");
        }
        else
        {
            res.json({
                result: false,
                message: "login required"
            });
        }
    }
}
