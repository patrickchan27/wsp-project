import * as express from "express";
import { Request, Response } from "express";
import { TypeService } from "../services/TypeService"


export class TypeRouter {

    constructor(private typeService: TypeService) {

    }

    router() {
        const router = express.Router();
        router.get("/", this.get);
        router.post("/", this.post);
        router.put("/", this.put);
        router.put("/:id/show/:show", this.updateTypeDisplay);
        router.delete("/", this.delete);
        return router;
    }

    get = (req: Request, res: Response) => {
        if (req.user && req.user["id"]) {
            res.json({
                result: true,
                list: this.typeService.getList(req.user["id"])
            });
        }
        else {
            res.status(401).json({
                result: false,
                message: "login required"
            });
        }
    }

    post = (req: Request, res: Response) => {
        if (req.user && req.user["id"]) {
            this.typeService.createType(req.user["id"], req.body.typeName);
            res.json({
                result: true
            });
        }
        else {
            res.status(401).json({
                result: false,
                message: "login required"
            });
        }
    }

    put = (req: Request, res: Response) => {
        if (req.user && req.user["id"]) {
            this.typeService.moveType(req.user["id"], req.query.typeId, req.query.moveUp == "true");
            res.json({
                result: true
            });
        }
        else {
            res.status(401).json({
                result: false,
                message: "login required"
            });
        }
    }

    updateTypeDisplay = (req: Request, res: Response) => {
        const id = parseInt(req.params.id);
        if (req.user && req.user["id"] && !isNaN(id) && (req.params.show == "true" || req.params.show == "false")) {
            this.typeService.updateTypeDisplay(req.user["id"], id, req.params.show == "true");
            res.json({ result: true });
        } else {
            res.status(500).json({ result: false });
        }
    }

    delete = (req: Request, res: Response) => {
        if (req.user && req.user["id"]) {
            this.typeService.deleteType(req.user["id"], req.query.typeId);
            res.json({
                result: true
            });
        }
        else {
            res.status(401).json({
                result: false,
                message: "login required"
            });
        }
    }

}
