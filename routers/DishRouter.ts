import * as path from 'path';

import * as express from "express";
import { Request, Response } from "express";
import { DishService } from "../services/DishService";
import * as multer from "multer";

export class DishRouter {
    private upload: multer.Instance

    constructor(private dishService: DishService) {
        const storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, path.join(__dirname, '/../', 'uploads', 'dishes', 'full'));
            },
            filename: function (req, file, cb) {
                cb(null, `dish-${Date.now()}.${file.mimetype.split('/')[1]}`);
            }
        });

        const limits = {
            fileSize: 3000000
        }

        this.upload = multer({ storage, limits })

    }

    router() {
        const router = express.Router();
        router.get("/", this.getAll);
        router.get("/:id", this.get);
        router.post("/", this.upload.single('image'), this.post);
        router.put("/:id", this.upload.single('image'), this.put);
        router.put("/:id/show/:show", this.updateDishDisplay);
        router.delete("/:id", this.delete);
        return router;
    }

    private getAll = async (req: Request, res: Response) => {
        try {
            if (req.user && req.user["id"]) {
                const dishes = await this.dishService.getDishes(req.user["id"]);
                res.json({ dishes });
            } else {
                res.status(401).json({ message: "Please login" });
            }
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "error" });
        }
    };

    private get = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            if (req.user && req.user["id"]) {
                const dish = await this.dishService.getDish(id, req.user["id"])
                if (dish) {
                    res.json(dish);
                } else {
                    res.status(404).json({ message: "Dish cannot be found" });
                }
            }
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "error" });
        }
    };

    private post = async (req: Request, res: Response) => {
        try {
            const { dish, type, description, dishPrice } = req.body;
            let options = req.body.options;
            if (options) {
                options = JSON.parse(options);
            }
            if (req.user && req.user["id"]) {
                const newDishID = await this.dishService.addDish(req.user["id"], dish, type, description, req.file.filename, dishPrice, options);

                res.json({ result: true, id: newDishID });
            } else {
                res.status(401).json({ message: "Please login" });
            }
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "error" });
        }
    };

    private put = async (req: Request, res: Response) => {        
        try {
            const { dish, type, description, dishPrice } = req.body;
            const dishID = parseInt(req.params.id)
            let options = req.body.options;
            if (options) {
                options = JSON.parse(options);
            }
            if (req.user && req.user["id"]) {
                if (req.file) {
                    await this.dishService.updateDish(dishID, req.user["id"], dish, type, description, req.file.filename, dishPrice, options);
                } else {
                    await this.dishService.updateDish(dishID, req.user["id"], dish, type, description, "", dishPrice, options);
                }
            } else {
                res.status(401).json({ message: "Please login" });
            }

            res.json({ result: true });
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "error" });
        }
    };

    private delete = async (req: Request, res: Response) => {
        try {
            const dishID = parseInt(req.params.id);
            if (req.user && req.user["id"] && dishID) {
                const result = await this.dishService.deleteDish(dishID, req.user["id"]);
                
                res.json({ result });
            } else {
                res.status(400).json({ message: "error" });
            }
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "error" });
        }
    };

    private updateDishDisplay = async (req: Request, res: Response) => {
        try {
            const dishID = parseInt(req.params.id);
            const show = req.params.show;

            if (req.user && req.user["id"] && dishID && (show == "true" || show == "false")) {
                await this.dishService.updateDishDisplay(dishID, req.user["id"], show == "true");
                res.json({ result: true });
            } else {
                res.status(500).json({ result: false });
            }
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "error" });
        }
    }
}