import * as express from "express";
import { Request, Response } from "express";
import { QRCodeService } from "../services/QRCodeService";
import { TableService } from "../services/TableService";

export class QRCodeRouter {
    constructor(private qRCodeService: QRCodeService, private tableService: TableService) {
    }

    router() {
        const router = express.Router();
        router.get("/tables", this.generateTableQRCodes);
        return router;
    }

    private generateTableQRCodes = async (req: Request, res: Response) => {
        try {
            if (req.user && req.user["id"]) {
                const zipPath = await this.qRCodeService.generateTableQRCodes(req.user["id"], this.tableService.getTables(req.user["id"])["tablesCount"]);
                // res.json({ links })

                res.download(zipPath);
            } else {
                res.status(401).json({ message: "Please login" });
            }
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "error" });
        }
    }
}