import * as express from "express";
import { Request, Response } from "express";
import { RestaurantService } from "../services/RestaurantService";
import { DishService } from "../services/DishService";
import { TypeService, Type } from "../services/TypeService";

export class RestaurantRouter
{

    constructor(private service: RestaurantService, private dishService: DishService, private typeService: TypeService) {}

    router()
    {
        const router = express.Router();
        router.get("/userId", this.getUserId);
        router.get("/sitdown/:restId/:tableId", this.sitdown);
        router.get("/standup/:tableId", this.standup);
        router.get("/menu", this.menu);
        router.get("/running", this.running);
        router.get("/ordered", this.getOrdered);
        router.post("/order", this.order);
        router.post("/start", this.start);
        router.post("/stop", this.stop);
        router.put("/doneOrder/:orderId", this.doneOrder);
        router.put("/retrieveOrder/:orderId", this.retrieveOrder);
        return router;
    }

    doneOrder = (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            res.json({
                result: this.service.doneOrder(req.user["id"], req.params.orderId)
            });
        }
        else
        {
            res.json({
                result: false,
                message: "login required"
            });
        }
    }

    retrieveOrder = (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            res.json({
                result: this.service.retrieveOrder(req.user["id"], req.params.orderId)
            });
        }
        else
        {
            res.json({
                result: false,
                message: "login required"
            });
        }
    }

    getOrdered = (req: Request, res: Response) =>
    {
        if (req.session && req.session.restId && req.session.tableCode)
        {
            let d = this.service.getOrdered(req.session.restId, req.session.tableCode, true);
            res.json({
                result: Boolean(d),
                list: d
            });
        }
        else if (req.user && req.user["id"])
        {
            let d = this.service.getOrdered(req.user["id"], req.query.tableId, false);
            res.json({
                result: Boolean(d),
                list: d
            });
        }
        else
        {
            res.json({
                result: false
            });
        }
    }

    running = (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            res.json({
                result: true,
                running: Boolean(this.service.getRestaurant(req.user["id"]))
            });
        }
        else
        {
            res.json({
                result: false,
                message: "login required"
            });
        }
    }

    start = (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            res.json({
                result: this.service.startRestaurant(req.user["id"])
            });
        }
        else
        {
            res.json({
                result: false,
                message: "login required"
            });
        }
    }

    stop = (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            res.json({
                result: this.service.stopRestaurant(req.user["id"])
            });
        }
        else
        {
            res.json({
                result: false,
                message: "login required"
            });
        }
    }

    getUserId = (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            res.json({
                result: true,
                userId: req.user["id"]
            });
        }
        else
        {
            res.json({
                result: false,
                message: "login required"
            });
        }
    }

    sitdown = (req: Request, res: Response) =>
    {
        let { restId, tableId } = req.params;
        let rest = this.service.getRestaurant(restId);
        if (rest && req.session)
        {
            req.session.restId = restId;
            let tableCode = this.service.sitdown(rest, tableId, req.session.tableCode);
            if (tableCode)
            {
                let t = rest.getTableWithCode(tableCode)
                req.session.tableCode = tableCode;
                res.redirect(`/order/index.html?restName=${this.service.getRestName(restId)}&table= + ${t ? t.getId() : ""}`);
                return;
            }
        }
        res.json({
            result: false
        });
    }

    standup = (req: Request, res: Response) =>
    {
        if (req.user && req.user["id"])
        {
            res.json({
                result: this.service.standup(req.user["id"], req.params.tableId)
            });
        }
        else
        {
            res.json({
                result: false,
                message: "login required"
            });
        }
    }

    menu = async (req: Request, res: Response) =>
    {
        if (req.session && req.session.restId && req.session.tableCode)
        {
            let rest = this.service.getRestaurant(req.session.restId);
            let t;
            if (rest)
            {
                t = rest.getTableWithCode(req.session.tableCode);
            }
            if (rest && t)
            {
                const dishes = await this.dishService.getDishes(parseInt(req.session.restId));
                let types = this.typeService.getList(req.session.restId);
                types = types.filter((e: Type) => e.show);
                for (let i = 0; i < types.length; i++)
                {
                    types[i].dishes = dishes.filter(e => e.type == types[i].id && e.show);
                }
                res.json({
                    result: true,
                    menu: types
                });
                return;
            }
        }
        res.json({
            result: false
        });
    }

    order = (req: Request, res: Response) =>
    {
        if (req.session && req.session.tableCode && req.body.items && req.body.items.length)
        {
            let rest = this.service.getRestaurant(req.session.restId);
            if (rest)
            {
                rest.order(req.session.tableCode, req.body.items);
                this.service.updateRestaurantOrders(rest.getId());
                res.json({
                    result: true
                });
                return;
            }
        }
        res.json({
            result: false
        });
    }

}
