import { Server } from "socket.io";
import { TableService, Tables } from "./TableService";
import { UserService } from "./UserService";
// import * as jsonfile from "jsonfile";
// import * as path from "path";


export class RestaurantService
{
    
    private restaurants: Restaurant[];

    constructor(private socket: Server, private tableService: TableService, private userService: UserService)
    {
        this.restaurants = [];
    }

    getRestName(restId: string)
    {
        let userInfo = this.userService.getUserInfo(Number(restId));
        if (userInfo)
        {
            return userInfo.restName;
        }
        return "";
    }

    startRestaurant(restId: string)
    {
        let index = this.restaurants.findIndex(e => e.getId() == restId);
        // let user = this.userService.getUsers().find(e => (e.id + "") == restId);
        this.tableService.emptyAll(restId);
        let tables = this.tableService.getTables(restId);
        if (index < 0 && tables)
        {
            this.restaurants.push(new Restaurant(restId, tables));
            return true;
        }
        return false;
    }

    stopRestaurant(restId: string)
    {
        let index = this.restaurants.findIndex(e => e.getId() == restId);
        if (index >= 0)
        {
            this.restaurants.splice(index, 1);
            return true;
        }
        return false;
    }

    getRestaurant(restId: string)
    {
        return this.restaurants.find(e => e.getId() == restId);
    }

    updateRestaurantOrders(restId: string)
    {
        let rest = this.getRestaurant(restId);
        if (rest)
        {
            this.socket.to(rest.getId()).emit("update-orders", rest.getOrders());
        }
    }

    sitdown(rest: Restaurant, tableId: string, tableCode: number)
    {
        let a = rest.sitdown(tableId, tableCode);
        if (a)
        {
            this.socket.to(rest.getId()).emit("update-empty-tables", this.getEmptyTables(rest.getId()));
        }
        return a;
    }

    standup(restId: string, tableId: string)
    {
        let rest = this.getRestaurant(restId);
        if (rest)
        {
            let t = rest.standup(tableId);
            if (t)
            {
                this.socket.to(restId).emit("update-empty-tables", this.getEmptyTables(restId));
            }
            return t;
        }
        return false;
    }

    getOrdered(restId: string, table: string, withCode: boolean)
    {
        let rest = this.getRestaurant(restId);
        if (rest)
        {
            return rest.getOrdered(table, withCode);
        }
        return undefined;
    }

    doneOrder(restId: string, orderId: string)
    {
        let rest = this.getRestaurant(restId);
        if (rest)
        {
            let b = rest.doneOrder(orderId);
            if (b)
            {
                this.socket.to(rest.getId()).emit("update-orders", rest.getOrders());
            }
            return b;
        }
        return false;
    }

    retrieveOrder(restId: string, orderId: string)
    {
        let rest = this.getRestaurant(restId);
        if (rest)
        {
            let b = rest.retrieveOrder(orderId);
            if (b)
            {
                this.socket.to(rest.getId()).emit("update-orders", rest.getOrders());
            }
            return b;
        }
        return false;
    }

    getEmptyTables(restId: string)
    {
        let rest = this.getRestaurant(restId);
        if (rest)
        {
            return rest.getEmptyTables();
        }
        return undefined;
    }

    updateEmptyTables(restId: string)
    {
        let rest = this.getRestaurant(restId);
        if (rest)
        {
            this.socket.to(rest.getId()).emit("update-empty-tables", rest.getEmptyTables());
        }
    }

}



type Item = {
    count: number,
    name: string,
    price: number,
    total: string,
    items: {
        name: string,
        price: string
    }
};

type Order = {
    id: string,
    tableNumber: string,
    orderTime: number,
    items: Item[]
};


class Restaurant
{

    private tables: Table[];

    private orders: Order[] = [];
    private history: Order[] = [];

    constructor(private id: string, tables: Tables)
    {
        this.tables = [];
        for (let i = 0; i < tables.pages.length; i++)
        {
            for (let j = 0; j < tables.pages[i].tables.length; j++)
            {
                this.tables.push(new Table(tables.pages[i].tables[j].number + ""));
            }
        }
    }

    sitdown(tableId: string, tableCode: number)
    {
        let ctable = this.tables.find(e => e.getCode() == tableCode);
        if (tableCode && ctable)
        {
            return tableCode;
        }
        let table = this.tables.find(e => tableId == e.getId());
        if (table)
        {
            return table.sitdown();
        }
        return 0;
    }

    order(tableCode: number, items: Item[])
    {
        let table = this.tables.find(e => tableCode == e.getCode());
        if (table)
        {
            let time = new Date().getTime();
            this.orders.push({
                id: Math.random() + "" + time,
                tableNumber: table.getId(),
                orderTime: time,
                items
            });
            table.order(items);
        }
    }

    getTableWithCode(code: number)
    {
        return this.tables.find(e => e.getCode() == code);
    }

    doneOrder(orderId: string)
    {
        let index = this.orders.findIndex(e => e.id == orderId);
        if (index >= 0)
        {
            this.history.push(this.orders[index]);
            this.orders.splice(index, 1);
            return true;
        }
        return false;
    }

    retrieveOrder(orderId: string)
    {
        let index = this.history.findIndex(e => e.id == orderId);
        if (index >= 0)
        {
            this.orders.push(this.history[index]);
            this.history.splice(index, 1);
            return true;
        }
        return false;
    }

    getOrders()
    {
        return {
            orders: this.orders,
            history: this.history
        };
    }

    getId()
    {
        return this.id;
    }

    getOrdered(table: string, withCode: boolean)
    {
        let t = this.tables.find(e => (withCode ? (e.getCode() + "" == table) : (e.getId() == table)));
        if (t)
        {
            return t.getOrdered();
        }
        return undefined;
    }

    standup(tableId: string)
    {
        let t = this.tables.find(e => e.getId() == tableId);
        if (t)
        {
            t.standup();
            return true;
        }
        return false;
    }

    getEmptyTables()
    {
        let a = [];
        for (let i = 0; i < this.tables.length; i++)
        {
            if (this.tables[i].isEmpty())
            {
                a.push(this.tables[i].getId());
            }
        }
        return a;
    }

}

class Table
{

    private orderedItems: Item[] = [];
    private empty = true;
    private code: number;

    constructor (private id: string)
    {
        
    }

    order(items: Item[])
    {
        this.orderedItems = this.orderedItems.concat(items);
    }

    sitdown()
    {
        if (this.empty)
        {
            this.empty = false;
            this.code = Math.random();
            return this.code;
        }
        return 0;
    }

    standup()
    {
        if (!this.empty)
        {
            this.empty = true;
            this.code = 0;
            this.orderedItems = [];
        }
    }

    getId()
    {
        return this.id;
    }

    getCode()
    {
        return this.code;
    }

    getOrdered()
    {
        return this.orderedItems;
    }

    isEmpty()
    {
        return this.empty;
    }

}


