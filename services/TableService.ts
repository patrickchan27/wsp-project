import * as path from "path";
import * as jsonfile from "jsonfile";

export type Table = {
    number: number,
    x: number,
    y: number,
    width: number,
    height: number,
    round: boolean
};

export type Tables = {
    tablesCount: number,
    pages: {
        tables: Table[]
    }[]
};

export class TableService
{

    private data = {};

    constructor()
    {
        this.readJsonFile();
    }

    private async readJsonFile() {
        try {
            this.data = await jsonfile.readFile(
                path.join(__dirname, "tables.json")
            );
        } catch (err) {
            throw err;
        }
    }

    private async writeJsonFile() {
        try {
            await jsonfile.writeFile(path.join(__dirname, "tables.json"), this.data, {
                spaces: 2
            });
        } catch (err) {
            throw err;
        }
    }

    setTables(userId: string, tables: Tables)
    {
        tables.tablesCount = 0;
        let tableId = 1;
        for (let i = 0; i < tables.pages.length; i++)
        {
            tables.tablesCount += tables.pages[i].tables.length;
            for (let j = 0; j < tables.pages[i].tables.length; j++)
            {
                tables.pages[i].tables[j].number = tableId;
                tableId++;
            }
        }
        this.data[userId] = tables;
        this.writeJsonFile();
    }

    getTables(userId: string): Tables
    {
        return this.data[userId];
    }

    emptyAll(userId: string)
    {
        if (this.data[userId])
        {
            for (let i = 0; i < this.data[userId].pages.length; i++)
            {
                for (let j = 0; j < this.data[userId].pages[i].tables.length; j++)
                {
                    this.data[userId].pages[i].tables[j].empty = true;
                }
            }
            this.writeJsonFile();
        }
    }

}

