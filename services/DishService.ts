import * as jsonfile from 'jsonfile';
import * as path from 'path';
import * as sharp from "sharp";
import * as fs from 'fs';

interface item {
    name: string;
    price: number;
}

type necessity = 'Required' | 'Optional';

interface option {
    title: string;
    necessity: necessity;
    maxItems: number;
    items: item[];
}

interface Dish {
    id: number;
    user: number;
    dish: string;
    type: string;
    show: boolean;
    description: string;
    image: string;
    dishPrice: string;
    options: option[] | null;
}

interface DishDataset {
    next_id: number;
    dishes: Dish[];
}

export class DishService {
    private async readJsonFile() {
        try {
            const dishesDataset: DishDataset = await jsonfile.readFile(
                path.join(__dirname, "dish.json")
            );
            return dishesDataset;
        } catch (err) {
            throw err;
        }
    }

    private async writeJsonFile(dataset: DishDataset) {
        try {
            await jsonfile.writeFile(path.join(__dirname, "dish.json"), dataset, {
                spaces: 2
            });
        } catch (err) {
            throw err;
        }
    }

    async addDish(user: number, dish: string, type: string, description: string, image: string, dishPrice: string, options: option[] | null) {
        try {
            const dishesDataset = await this.readJsonFile();

            if (image) {
                const width = 500;
                const height = 500;
                const fullImagePath = path.join(__dirname, '/../', 'uploads', 'dishes', 'full', image);
                const newImagePath = path.join(__dirname, '/../', 'storages', 'images', 'dishes', path.basename(fullImagePath, path.extname(fullImagePath)) + '.jpg');
                await sharp(fullImagePath).resize(width, height, { fit: 'contain', background: '#FFF' }).flatten({ background: '#FFF' }).jpeg().toFile(newImagePath);
                image = path.basename(newImagePath, path.extname(newImagePath)) + '.jpg';
            }

            if (options) {
                for (const option of options) {
                    option.title = option.title.trim();
                    for (const item of option.items) {
                        item.name = item.name.trim();
                    }
                }
            }

            const dishObject = {
                id: dishesDataset.next_id,
                user,
                dish: dish.trim(),
                type,
                show: true,
                description: description.trim(),
                image,
                dishPrice,
                options,
            };
            dishesDataset.dishes.push(dishObject);
            dishesDataset.next_id += 1;
            await this.writeJsonFile(dishesDataset);

            return dishesDataset.next_id - 1;
        } catch (err) {
            throw err;
        }
    }

    async getDishes(user: number) {
        try {
            const dishesDataset = await this.readJsonFile();
            const userDishes = dishesDataset.dishes.filter((dish) => dish.user === user)
            return userDishes;
        } catch (err) {
            throw err;
        }
    }

    async getDish(id: number, user: number) {
        try {
            const dishesDataset = await this.readJsonFile();
            return dishesDataset.dishes.find((dish) => dish.id === id && dish.user === user);
        } catch (err) {
            throw err;
        }
    }

    async updateDish(id: number, user: number, dish: string, type: string, description: string, image: string, dishPrice: string, options: option[] | null) {
        try {
            const dishesDataset = await this.readJsonFile();

            const dishIndex = dishesDataset.dishes.findIndex((dish) => dish.id === id && dish.user === user);
            if (dishIndex === -1) {
                throw 'Dish is no found.'
            }

            const dishObject = dishesDataset.dishes[dishIndex];

            if (image) {
                const width = 500;
                const height = 500;
                const fullImagePath = path.join(__dirname, '/../', 'uploads', 'dishes', 'full', image);
                const newImagePath = path.join(__dirname, '/../', 'storages', 'images', 'dishes', path.basename(fullImagePath, path.extname(fullImagePath)) + '.jpg');
                await sharp(fullImagePath).resize(width, height, { fit: 'contain', background: '#FFF' }).flatten({ background: '#FFF' }).jpeg().toFile(newImagePath);
                image = path.basename(newImagePath, path.extname(newImagePath)) + '.jpg';
            }

            if (options) {
                for (const option of options) {
                    option.title = option.title.trim();
                    for (const item of option.items) {
                        item.name = item.name.trim();
                    }
                }
            }

            // console.log(dish);

            dishObject.dish = dish.trim();
            dishObject.type = type;
            dishObject.description = description.trim();
            dishObject.dishPrice = dishPrice;
            dishObject.options = options;

            if (image) {
                const oldDishImage = dishObject.image;
                dishObject.image = image;
                await this.deleteDishImage(oldDishImage);
            }

            await this.writeJsonFile(dishesDataset);
        } catch (err) {
            throw err;
        }
    }

    async deleteDish(id: number, user: number) {
        try {
            const dishesDataset = await this.readJsonFile();
            const index = dishesDataset.dishes.findIndex((dish) => dish.id === id && dish.user === user);

            if (index >= 0) {
                await this.deleteDishImage(dishesDataset.dishes[index].image);
                dishesDataset.dishes.splice(index, 1);
            } else {
                return false;
            }

            await this.writeJsonFile(dishesDataset);

            return true;
        } catch (err) {
            throw err;
        }
    }

    async updateDishDisplay(id: number, user: number, show: boolean) {
        try {
            const dishesDataset = await this.readJsonFile();
            const index = dishesDataset.dishes.findIndex((dish) => dish.id === id && dish.user === user);
            if (index >= 0) {
                dishesDataset.dishes[index].show = show
            } else {
                return false;
            }

            await this.writeJsonFile(dishesDataset);

            return true;
        } catch (err) {
            throw err;
        }
    }

    private async deleteDishImage(image: string) {
        try {
            const filePath = path.join(__dirname, '/../', 'storages', 'images', 'dishes', image)
            if (fs.existsSync(filePath)) {
                await fs.promises.unlink(filePath);
            }
        } catch (err) {
            throw err;
        }
    }
}