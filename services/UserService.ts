import * as jsonfile from 'jsonfile';
import * as path from 'path';
import { hashPassword } from '../hash';

export interface User {
    id: number;
    googleUser: boolean;
    restName: string;
    email: string;
    username: string;
    password: string;
}

export class UserService {

    private users: User[];
    private currentId: number;

    constructor()
    {
        this.readJsonFile();
    }

    private async readJsonFile() {
        try {
            const obj = await jsonfile.readFile(
                path.join(__dirname, "users.json")
            );
            this.users = obj.users;
            this.currentId = obj.currentId;
        } catch (err) {
            throw err;
        }
    }

    private async writeJsonFile() {
        try {
            await jsonfile.writeFile(path.join(__dirname, "users.json"), {
                users: this.users,
                currentId: this.currentId
            }, {
                spaces: 2
            });
        } catch (err) {
            throw err;
        }
    }

    register(user: User)
    {
        if (this.users.find(e => e.email == user.email || e.username == user.username))
        {
            return false;
        }
        else
        {
            this.currentId++;
            user.id = this.currentId;
            this.users.push(user);
            this.writeJsonFile();
            return true;
        }
    }

    async changeInfo(id: number, restName: string, email: string, username: string, password: string, cpassword: string)
    {
        let user = this.users.find(e => e.id == id)
        if (user)
        {
            let result = true;
            if (restName)
            {
                user.restName = restName;
            }
            if (email)
            {
                let i = this.users.findIndex(e => e.email == email);
                if (!(i >= 0 && this.users[i] !== user))
                {
                    user.email = email;
                }
                else
                {
                    result = false;
                }
            }
            if (username)
            {
                let i = this.users.findIndex(e => e.username == username);
                if (!(i >= 0 && this.users[i] !== user))
                {
                    user.username = username;
                }
                else
                {
                    result = false;
                }
            }
            if (password && cpassword)
            {
                if (password === cpassword)
                {
                    user.password = await hashPassword(password);
                }
                else
                {
                    result = false;
                }
            }
            this.writeJsonFile();
            return result;
        }
        return false;
    }

    getUserInfo(id: number)
    {
        let user = this.users.find(e => e.id == id);
        if (user)
        {
            return {
                restName: user.restName,
                email: user.email,
                username: user.username
            };
        }
        return;
    }

    getUsers()
    {
        return this.users;
    }

}
