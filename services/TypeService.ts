import * as path from "path";
import * as jsonfile from "jsonfile";

export type Type = {
    id: number,
    name: string,
    show: true,
};

export class TypeService {

    private data = {};
    private currentId: number;

    constructor() {
        this.readJsonFile();
    }

    private async readJsonFile() {
        try {
            const obj = await jsonfile.readFile(
                path.join(__dirname, "types.json")
            );
            this.data = obj.data;
            this.currentId = obj.currentId;
        } catch (err) {
            throw err;
        }
    }

    private async writeJsonFile() {
        try {
            await jsonfile.writeFile(path.join(__dirname, "types.json"), {
                data: this.data,
                currentId: this.currentId
            }, {
                spaces: 2
            });
        } catch (err) {
            throw err;
        }
    }

    createType(userId: string, typeName: string) {
        if (!this.data[userId]) {
            this.data[userId] = [];
        }
        if (typeName && !this.data[userId].find((e: Type) => e.name == typeName)) {
            this.currentId++;
            this.data[userId].push({
                id: this.currentId,
                name: typeName,
                show: true,
            });
        }
        this.writeJsonFile();
    }

    deleteType(userId: string, id: number) {
        let index = this.data[userId].findIndex((e: Type) => e.id == id);
        if (index >= 0) {
            this.data[userId].splice(index, 1);
        }
        this.writeJsonFile();
    }

    moveType(userId: string, id: number, up: boolean) {
        let index = this.data[userId].findIndex((e: Type) => e.id == id);
        if (up ? index > 0 : index < this.data[userId].length - 1) {
            let temp = this.data[userId][index];
            this.data[userId][index] = this.data[userId][index + (up ? -1 : 1)];
            this.data[userId][index + (up ? -1 : 1)] = temp;
        }
        this.writeJsonFile();
    }

    updateTypeDisplay(userId: string, id: number, show: boolean) {
        let index = this.data[userId].findIndex((e: Type) => e.id == id);
        if (index >= 0) {
            this.data[userId][index].show = show;
        }
        this.writeJsonFile();
    }

    getList(userId: number) {
        return JSON.parse(JSON.stringify(this.data[userId]));
    }

}