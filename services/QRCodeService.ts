import * as dotenv from "dotenv";
import * as path from "path";
import * as fs from "fs";
import * as JSZip from "jszip";

const QRCode = require('qrcode');
const base64Img = require('base64-img');

interface QRCodeImage {
    link: string;
    fileName: string;
}

export class QRCodeService {
    private generateQR = async (link: string): Promise<string> => {
        const opts = {
            errorCorrectionLevel: 'H',
            width: 200
        }

        try {
            return QRCode.toDataURL(link, opts)
        } catch (err) {
            throw err;
        }
    }

    private saveQRCode = async (dataURL: string, filename: string) => {
        return base64Img.img(dataURL, path.join(__dirname, '../', 'storages/', 'qrcode/'), filename, function (err: Error) {
            if (err) throw err;
        });
    }

    toFiles = async (files: QRCodeImage[] | QRCodeImage) => {
        try {
            if (Array.isArray(files)) {
                const dataURLs = await Promise.all(files.map(file => {
                    return this.generateQR(file.link);
                }))

                const dataURLsPromises = dataURLs.map((dataURL, index) => {
                    return this.saveQRCode(dataURL, files[index].fileName);
                })

                await Promise.all(dataURLsPromises)

            } else {
                const dataURL = await this.generateQR(files.link);
                await this.saveQRCode(dataURL, files.fileName);
            }
        } catch (error) {
            throw error;
        }
    }

    generateTableQRCodes = async (user: number, tableCount: number) => {
        // {hostname}/sitdown/{user}/{tableID}

        dotenv.config();
        const { HOSTNAME } = process.env;

        try {
            const files: QRCodeImage[] = [];
            const filesPath: string[] = [];
            for (let tableID = 1; tableID <= tableCount; tableID++) {
                files.push({
                    link: `${HOSTNAME}rest/sitdown/${user}/${tableID}`,
                    fileName: `${user}-table-${tableID}`,
                })

                // links.push(`${HOSTNAME}images/qrcode/${user}-table-${tableID}.png`)
                filesPath.push(path.join(__dirname, '../', 'storages/', 'qrcode/', `${user}-table-${tableID}.png`))
            }

            await this.toFiles(files);

            const zip = new JSZip();
            const zipPath = path.join(__dirname, '../', 'storages/', 'qrcode/', `QR Code - ${user}.zip`);
            for (let tableID = 0; tableID < tableCount; tableID++) {
                const filePath = filesPath[tableID]
                const fileBuffer = await fs.promises.readFile(filePath);
                
                zip.file(`table-${tableID + 1}.png`, fileBuffer);
            }

            await new Promise(function(resolve, reject) { 
                zip.generateNodeStream({ type: 'nodebuffer', streamFiles: true })
                .pipe(fs.createWriteStream(zipPath))
                .on('finish', function () {
                    resolve();
                });
            })

            return zipPath;
        } catch (error) {
            throw error;
        }
    }
}