class Rest
{
    tables = [];
    ratio = 1;
    rest = {};
    scale = 1;

    constructor(ratio)
    {
        this.ratio = ratio;
    }

    render()
    {
        let canvasRatio = canvas.height / canvas.width;
 
        this.rest = this.ratio < canvasRatio ? {
            w: canvas.width * 0.98,
            h: canvas.width * 0.98 * this.ratio
        } : {
            h: canvas.height * 0.98,
            w: canvas.height * 0.98 / this.ratio
        };

        this.rest.x = (canvas.width - this.rest.w) / 2;
        this.rest.y = (canvas.height - this.rest.h) / 2;

        ctx.beginPath();
        ctx.rect(this.rest.x, this.rest.y, this.rest.w, this.rest.h);
        ctx.strokeStyle = "lightgray";
        ctx.lineWidth = 3;
        ctx.stroke();

        this.scale = Math.sqrt(this.rest.w * this.rest.w + this.rest.h * this.rest.h) / 100;

        for (let i = 0; i < this.tables.length; i++)
        {
            this.tables[i].render(this.rest, this.scale);
        }
    }

    mouseMoved(e)
    {
        let x = e.layerX - this.rest.x;
        let y = e.layerY - this.rest.y;
        if (x > 0 && x < this.rest.w && y > 0 && y < this.rest.h)
        {
            for (let i = 0; i < this.tables.length; i++)
            {
                this.tables[i].hovering(x, y, this.rest, this.scale);
            }
        }
    }

    drag(e)
    {
        let x = e.layerX - this.rest.x;
        let y = e.layerY - this.rest.y;
        let table = this.tables.find(e => e.hover);
        if (table)
        {
            table.x = x / this.rest.w;
            table.y = y / this.rest.h;
        }
    }

    async launch()
    {
        if (!obj.running && !obj.editMode)
        {
            return;
        }
        let table = this.tables.find(e => e.hover);
        if (table)
        {
            $(`#${obj.editMode ? "tsm" : "tim"}Launch`).click();
            $(`#${obj.editMode ? "tsetting" : "tinfo"}Modalbody`).html(obj.editMode ? this.tableSettingHtml(table) : (await this.tableInfoHtml(table)));
            $(`#${obj.editMode ? "tsetting" : "tinfo"}ModalTitle`).html((obj.editMode ? "Edit Table " : "Table ") + table.number);
        }
    }

    tableSettingHtml(table)
    {
        obj.edittingTable = table;
        return `
        <form id="table-setting-form"javascript:saveTableSettings()>
            Width (Radius):
            <input type="number" size="3" style="width:3em;" value="${table.width}" name="width">
            <br>
            Height:
            <input type="number" size="3" style="width:3em;" value="${table.height}" name="height">
            <br>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="round" ${table.round ? 'checked="true"' : ""} name="round">
                
                <label class="custom-control-label" for="round">
                    Round Table
                </label>
            </div>
        </form>
        `;
    }

    async tableInfoHtml(table)
    {
        if (!table.empty)
        {
            const ordered = await (await fetch("/rest/ordered?tableId=" + table.number)).json();
            obj.currentTableInfo = table.number;
            if (ordered.list.length > 0)
            {
                let total = 0;
                for (let i = 0; i < ordered.list.length; i++)
                {
                    total += ordered.list[i].price * ordered.list[i].count;
                    if (ordered.list[i].items)
                    {
                        for (let j = 0; j < ordered.list[i].items.length; j++)
                        {
                            total += Number(ordered.list[i].items[j].price);
                        }
                    }
                }
                return `
                ${loopItems(ordered.list)}
                <hr>
                <span class="text-muted">Total:</span>
                <h2 class="float-right">$${total}</h2>
                `;
            }
            else
            {
                return "Customer haven't ordered yet.";
            }
        }
        else
        {
            return "This table is empty";
        }
    }
}

function loopItems(items)
{
    let html = "";
    for (let i = 0; i < items.length; i++)
    {
        html += `
        <div>
            ${items[i].name} x ${items[i].count}
            <span class="float-right">
                $${items[i].total}
            </span>
            ${loopOptions(items[i].items)}
        </div>
        `;
    }
    return html;
}

function loopOptions(options)
{
    if (options)
    {
        let html = "<ul>";
        for (let i = 0; i < options.length; i++)
        {
            html += `
            <li>
                ${options[i].name}
                <span class="float-right">
                    + $${options[i].price}
                </span>
            </li>
            `;
        }
        return html;
    }
    return "";
}