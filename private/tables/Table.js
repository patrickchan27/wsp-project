class Table
{
    number = 0;

    x = 0;
    y = 0;
    width = 0;
    height = 0;
    round = false;

    hover = false;

    empty = true;

    constructor(number, x, y, width, height, round)
    {
        this.number = number;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = round ? width : height;
        this.round = round;
    }

    render(rest, scale)
    {
        let x = this.x * rest.w + rest.x;
        let y = this.y * rest.h + rest.y;
        let width = this.width * scale;
        let height = this.height * scale;
        ctx.beginPath();

        ctx.fillStyle = obj.running ? (this.empty ? `rgba(50, 200, 50, ${this.hover ? "0.5" : "0.75"})` : `rgba(200, 50, 50, ${this.hover ? "0.5" : "0.75"})`) : "gray";
        ctx.strokeStyle = obj.running ? (this.empty ? `rgba(50, 200, 50)` : `rgba(200, 50, 50)`) : "gray";
        ctx.lineWidth = 3;

        if (this.round)
        {
            ctx.arc(x, y, width / 2, 0, 2 * Math.PI);
        }
        else
        {
            ctx.rect(x - width / 2, y - height / 2, width, height);
        }
        ctx.fill();
        ctx.stroke();

        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillStyle = "white";
        ctx.font = 1.5 * scale + "px consolas";
        ctx.fillText(this.number, x, y);
    }

    hovering(x, y, rest, scale)
    {
        let tx = this.x * rest.w;
        let ty = this.y * rest.h;
        let width = this.width * scale;
        let height = this.height * scale;

        let dx = Math.abs(x - tx);
        let dy = Math.abs(y - ty);
        this.hover = (this.round ? (Math.sqrt(dx * dx + dy * dy) < width / 2) : (dx < width / 2 && dy < height / 2));
    }
}