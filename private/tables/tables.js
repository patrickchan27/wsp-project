
const socket = io.connect();

async function joinRoom()
{
    const res = await fetch('/rest/userId');
    const obj = await res.json();
    socket.emit("join", obj.userId);
}
joinRoom();

socket.on('update-empty-tables', async data => {
    await getTableData();
    for (let i = 0; i < obj.pages.length; i++)
    {
        for (let j = 0; j < obj.pages[i].tables.length; j++)
        {
            obj.pages[i].tables[j].empty = data.includes(obj.pages[i].tables[j].number + "");
        }
    }
    render();
});

canvas = document.getElementById('canvas');
ctx = canvas.getContext('2d');
ctx.translate(0.5, 0.5);

let obj = {
    editMode: false,
    pages: [new Rest(1)],
    currentPage: 0,

    mousedown: false,
    dragging: false,

    getRest: () => {
        if (!obj.pages[obj.currentPage])
        {
            obj.pages[obj.currentPage] = new Rest(1);
        }
        return obj.pages[obj.currentPage]
    }
};

function render()
{
    canvas.width = $(canvas).width();
    canvas.height = $(canvas).height();

    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    obj.getRest().render();
}

render();

$(canvas).on("mousemove", e => {
    if (obj.mousedown)
    {
        obj.dragging = true;
        if (obj.editMode)
        {
            obj.getRest().drag(e.originalEvent);
        }
    }
    else
    {
        obj.getRest().mouseMoved(e.originalEvent);
    }
    render();
});

$(canvas).on("mousedown", e => {
    obj.mousedown = true;
});

$(canvas).on("mouseup", e => {
    if (!obj.dragging)
    {
        obj.getRest().launch();
    }
    obj.dragging = false;
    obj.mousedown = false;
});

$(window).on('resize', () => {
    $("#canvas").height($(window).innerHeight() - $("nav").outerHeight() - $("#header").outerHeight());
    render();
})
.trigger('resize');


function edit()
{
    if (!obj.running)
    {
        obj.editMode = !obj.editMode;
        if (!obj.editMode)
        {
            uploadTableData();
            getTableData();
        }
        $("#buttons").html(`
        <button class="btn btn-${obj.editMode ? "success" : "danger"} float-right" onclick="edit()">${obj.editMode ? "Done" : "Edit"}</button>
        ${obj.editMode ? `
        <button class="btn btn-secondary float-right mr-1" onclick="newTable()">Add Table</button>
        <button class="btn btn-secondary float-right mr-1" onclick="settings()">Settings</button>
        ` : ""}
        
        `);
    }
    else
    {
        alert("Cannot edit while Restaurant is running.");
    }
}

function settings()
{
    let r = obj.getRest();
    $("#gsettingModalbody").html(`
    <form id="gsetting-form" action="javascript:saveGSettings()">
        Size Ratio:
        <input type="number" style="width:3em;" value="${r.ratio}" name="ratio" min="0.1" max="10" step="0.1">
    </form>
    `);
    $("#gsmLaunch").click();
}

function saveGSettings()
{
    let form = $("#gsetting-form")[0];
    if (form)
    {
        let r = obj.getRest();
        r.ratio = form.ratio.value;
        render();
    }
}

function saveTableSettings()
{
    let form = $("#table-setting-form")[0];
    if (form)
    {
        obj.edittingTable.width = form.width.value;
        obj.edittingTable.height = form.height.value;
        obj.edittingTable.round = form.round.checked;
        render();
    }
}

function removeTable()
{
    obj.getRest().tables = obj.getRest().tables.filter(e => e.number != obj.edittingTable.number);
    render();
}

function newTable()
{
    if (obj.edittingTable)
    {
        obj.getRest().tables.push(new Table(0, 0.5, 0.5, obj.edittingTable.width, obj.edittingTable.height, obj.edittingTable.round));
    }
    else
    {
        obj.getRest().tables.push(new Table(0, 0.5, 0.5, 2, 2, false));
    }
}

async function payed(tableId)
{
    const res = await fetch("/rest/standup/" + tableId);
    console.log(await res.text());
}

async function getTableData()
{
    const data = (await (await fetch("/tables/data")).json()).tables;
    obj.running = (await (await fetch("/rest/running")).json()).running;
    if (data && data.pages)
    {
        obj.pages = [];
        for (let i = 0; i < data.pages.length; i++)
        {
            obj.pages[i] = new Rest(data.pages[i].ratio);
            for (let j = 0; j < data.pages[i].tables.length; j++)
            {
                let t = data.pages[i].tables[j];
                obj.pages[i].tables.push(new Table(t.number, t.x, t.y, t.width, t.height, t.round));
            }
        }
    }
    render();
}
getTableData();

async function uploadTableData()
{
    obj.pages = obj.pages.filter(e => e.tables.length > 0);
    const res = await fetch('/tables', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            tables: {
                tablesCount: obj.tablesCount,
                pages: obj.pages
            }
        })
    });
    console.log(await res.json());
}


$("#pageInput").on("change", function() {
    obj.currentPage = this.value - 1;
    render();
});
