let dishID = 0;

(async () => {
    await listTypeOptions();

    const urlParams = new URLSearchParams(window.location.search);
    const paramsID = parseInt(urlParams.get('id'));
    if (urlParams.get('id') && typeof paramsID == 'number') {
        await getDishInfo(urlParams.get('id'))
    }
})();

async function listTypeOptions() {
    const res = await fetch('../types');
    const resJson = await res.json();
    const select = document.querySelector("#type");

    if (resJson.list === undefined) {
        window.location.replace("../types/types.html?error=NoType");
    }

    for (let i = 0; i < resJson.list.length; i++) {
        const type = resJson.list[i];
        const option = document.createElement("option");
        option.text = type.name;
        option.value = type.id;
        select.add(option);
    }
}

async function getDishInfo(id) {
    const res = await fetch(`../dish/${id}`);
    const resJson = await res.json();

    if (res.status === 200) {
        const dishForm = document.querySelector("#dish-form");
        dishID = resJson.id;
        dishForm.querySelector("#dish").value = resJson.dish;
        dishForm.querySelector("#type").value = resJson.type;
        dishForm.querySelector("#description").value = resJson.description;
        dishForm.querySelector("#dishPrice").value = resJson.dishPrice;

        const imageCropper = document.querySelector("#imageCropper");
        imageCropper.src = `../images/dishes/${resJson.image}`;
        imageCropper.classList.remove("d-none");

        dishForm.querySelector("#imageUpload").removeAttribute("required");

        // if (imageCropper.cropper) {
        //     imageCropper.cropper.destroy();
        // }

        // new Cropper(imageCropper, {
        //     aspectRatio: 1,
        //     dragMode: 'move',
        //     autoCropArea: 1,
        // })

        for (const option of resJson.options) {
            createOption()
            const optionNode = document.querySelectorAll(".option")[document.querySelectorAll(".option").length - 1];
            optionNode.querySelector("input[name='title']").value = option.title;
            optionNode.querySelector(".option-title").innerHTML = option.title;
            optionNode.querySelector("select[name='necessity']").value = option.necessity;
            optionNode.querySelector("input[name='maxItems']").value = option.maxItems;

            for (let i = 0; i < option.items.length; i++) {
                const item = option.items[i];
                if (i > 0) {
                    createItem(optionNode);
                }

                const itemNode = optionNode.querySelectorAll(".item")[optionNode.querySelectorAll(".item").length - 1];

                itemNode.querySelector("input[name='itemName']").value = item.name
                itemNode.querySelector("input[name='itemPrice']").value = item.price
            }
        }
    }
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
}

function createOption() {
    const newOption = document.querySelector(".option-copy").cloneNode(true);
    newOption.classList.remove("option-copy");
    newOption.classList.add("option");

    document.querySelector("#additionalOption").appendChild(newOption);

    new Sortable(newOption.querySelector(".items"), {
        animation: 150,
        ghostClass: 'blue-background-class'
    });

    newOption.querySelector(".option-info-collapse").addEventListener("click", function (e) {
        if (this.classList.contains("fa-chevron-up")) {
            newOption.querySelector(".option-info").classList.remove("d-block");
            newOption.querySelector(".option-info").classList.add("d-none");
            this.classList.remove("fa-chevron-up");
            this.classList.add("fa-chevron-down");
        } else {
            newOption.querySelector(".option-info").classList.add("d-block");
            newOption.querySelector(".option-info").classList.remove("d-none");
            this.classList.add("fa-chevron-up");
            this.classList.remove("fa-chevron-down");
        }
    })

    newOption.querySelector(".option-remove").addEventListener("click", function (e) {
        newOption.remove();
    })

    newOption.querySelector('button[name="addItem"]').addEventListener("click", function (e) {
        createItem(newOption)
    })

    const defaultItem = newOption.querySelector(".item");

    newOption.querySelector('button[name=deleteItem]').addEventListener('click', function (e) {
        deleteItem(defaultItem)
    })

    newOption.querySelector('input[name=title]').addEventListener("change", function (e) {
        const optionTitle = newOption.querySelector('.option-title')
        if (this.value) {
            optionTitle.innerHTML = this.value;
        } else {
            optionTitle.innerHTML = "Option";
        }
    })
}

function createItem(optionNode) {
    const newItem = document.querySelector(".item-copy").cloneNode(true);
    newItem.classList.remove("item-copy");

    optionNode.querySelector(".items").appendChild(newItem);
    newItem.querySelector('button[name=deleteItem]').addEventListener('click', function (e) {
        deleteItem(newItem)
    })
}

function deleteItem(itemNode) {
    if (itemNode.parentNode.childElementCount > 1) {
        itemNode.parentNode.removeChild(itemNode);
    }
}

document.querySelector("#btnAddOptions").addEventListener('click', function (e) {
    createOption();
});

document.querySelector("#imageUpload").addEventListener('change', async function (e) {
    if (this.files[0]) {
        let reader = new FileReader();
        const imageCropper = document.querySelector("#imageCropper");

        reader.onload = function (e) {
            imageCropper.src = e.target.result;
            imageCropper.classList.remove("d-none");

            if (imageCropper.cropper) {
                imageCropper.cropper.destroy();
            }

            new Cropper(imageCropper, {
                aspectRatio: 1,
                dragMode: 'move',
                autoCropArea: 1,
            })
        }
        reader.readAsDataURL(this.files[0]);
    }
})

document.querySelector("#dish-form").addEventListener('submit', async function (e) {
    e.preventDefault();

    let formData = new FormData();
    formData.append('dish', this.querySelector("input[name='dish']").value);    
    formData.append('type', this.querySelector("select[name='type']").value);
    formData.append('description', this.querySelector("textarea[name='description']").value);
    formData.append('dishPrice', this.querySelector("input[name='dishPrice']").value);

    if (this.querySelector("input[name='imageUpload']").files[0]) {
        const ImageURL = this.querySelector("#imageCropper").cropper.getCroppedCanvas({
            fillColor: '#fff'
        }).toDataURL('image/jpeg', 1)

        // Split the base64 string in data and contentType
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

        // Convert it to a blob to upload
        var blob = b64toBlob(realData, contentType);

        formData.append('image', blob);
    } else {
        formData.append('image', "");
    }

    const options = [];
    for (const optionNode of this.querySelectorAll(".option")) {
        const option = {
            title: optionNode.querySelector("input[name='title']").value,
            necessity: optionNode.querySelector("select[name='necessity']").value,
            maxItems: optionNode.querySelector("input[name='maxItems']").value,
        };

        const items = [];
        for (const itemNode of optionNode.querySelectorAll(".item")) {
            items.push({
                name: itemNode.querySelector("input[name='itemName']").value,
                price: itemNode.querySelector("input[name='itemPrice']").value,
            })
        }

        option['items'] = items;
        options.push(option);
    }
    formData.append("options", JSON.stringify(options))

    let httpMethod = 'POST';
    let submitLink = '../dish';
    if (dishID !== 0) {
        httpMethod = 'PUT';
        submitLink = submitLink + `/${dishID}`;
    }    
    
    const res = await fetch(submitLink, {
        method: httpMethod,
        body: formData
    });

    if (res.status === 200) {
        alert('Submitted!');
        if (dishID === 0) {
            const resJson = await res.json()
            window.location = '../dish/dish.html?id=' + resJson.id;
        }
    } else {
        // const resJson = await res.json();
        // alert(resJson.message);
        alert('error!');
    }
});

bsCustomFileInput.init();

new Sortable(document.querySelector("#additionalOption"), {
    animation: 150,
    ghostClass: 'blue-background-class'
});

