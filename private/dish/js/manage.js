

list = [];



function displayList(filter) {
    if (typeof filter == "string") {
        filter = filter.toUpperCase();
    }
    let html = "";
    for (let i = 0; i < list.length; i++) {
        if (!filter || (filter && typeof list[i].dish == "string" && list[i].dish.toUpperCase().includes(filter))) {
            html += `
            <div class="d-flex flex-column flex-lg-row justify-content-lg-between align-items-lg-center border-bottom p-3">
                <div class="custom-control custom-checkbox my-1">
                    <input type="checkbox" class="custom-control-input" onclick="showDish(customCheck${i}, ${list[i].id})" id="customCheck${i}" ${list[i].show ? "checked" : ""}>
                    <label class="custom-control-label h5" for="customCheck${i}">${list[i].dish}</label>
                </div>
                <div class="my-1">
                    <button class="btn btn-secondary" onclick="moveDishDown(${i})"><i class="fas fa-chevron-down"></i></button>
                    <button class="btn btn-secondary ml-2" onclick="moveDishUp(${i})"><i class="fas fa-chevron-up"></i></button>
                    <a class="btn btn-info ml-2" href="./dish.html?id=${list[i].id}" role="button">Edit</a>
                    <button class="btn btn-danger ml-2" onclick="deleteDish(${list[i].id})">Delete</button>
                </div>
            </div>
            `;
        }
    }

    $("#list-container").html(html);
}

function moveDishUp(index) {
    if (index > 0) {
        let temp = list[index - 1];
        list[index - 1] = list[index];
        list[index] = temp;
        displayList();
    }
}

function moveDishDown(index) {
    if (index < list.length - 1) {
        let temp = list[index + 1];
        list[index + 1] = list[index];
        list[index] = temp;
        displayList();
    }
}

async function downloadDishesList() {
    const res = await fetch('http://localhost:8080/dish');
    const obj = await res.json();
    list = obj.dishes;
    displayList();
}

async function deleteDish(dishID) {
    const res = await fetch('http://localhost:8080/dish/' + dishID, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({})
    });
    if (res.status == 200) {
        downloadDishesList();
    }
}

async function showDish(checkbox, dishID) {
    if (checkbox) {
        const res = await fetch(`http://localhost:8080/dish/${dishID}/show/${checkbox.checked}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            }
        });
        if (res.status == 200) {
            downloadDishesList();
        }
    }
}

$("#search-input").keyup(function () {
    displayList(this.value);
});

downloadDishesList()

