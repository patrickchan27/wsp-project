

async function getState()
{
    let res = await (await fetch("/rest/running")).json();
    $("#state").html(`State: ${res.running ? "Running<button class='btn btn-danger ml-3' onclick='startOrStopRest()'>Stop</button>"
        : "Stopped<button class='btn btn-success ml-3' onclick='startOrStopRest(true)'>Start</button>"}`);
}
getState();

async function getUserInfo()
{
    let res = await (await fetch("/users/info")).json();
    $("#userinfo").html(`
    Restaurant Name: ${res.info.restName}<br>
    Username: ${res.info.username}<br>
    E-mail: ${res.info.email}<br>
    `);
}
getUserInfo();


async function startOrStopRest(start)
{
    let res = await (await fetch("/rest/" + (start ? "start" : "stop"), {method: "POST"})).json();
    if (res.result)
    {
        $("#errorMessage").hide();
    }
    else
    {
        $("#errorMessage").show();
    }
    getState();
}
$("#errorMessage").hide();


