
const socket = io.connect();

async function joinRoom()
{
    const res = await fetch('/rest/userId', {method: "GET"});
    const obj = await res.json();
    socket.emit("join", obj.userId);
}
joinRoom();

socket.on('update-orders', data => {
    orders = data.orders;
    history = data.history;
    displayOrders();
});


let orders = [];
let history = [];

let switchHistory = false;
let filterTable = false;
let filterTableNum = 1;


function formatTime(time, s1, s2)
{
    let date = new Date(time);
    return date.toISOString().substr(s1, s2);
}

function displayOrders()
{
    let loopOrders = orders;
    if (switchHistory)
    {
        loopOrders = history;
    }
    let html = "";
    for (let i = 0; i < loopOrders.length; i++)
    {
        if (filterTable && filterTableNum != loopOrders[i].tableNumber)
        {
            continue;
        }
        let time = new Date().getTime() - loopOrders[i].orderTime;
        let o = time >= 900 * 1000 && !switchHistory;
        html += `
        <div class="col-md-5 col-lg-4 col-xl-2 m-2 p-0 ${o ? "overtime" : ""}">
            <div class="shadow p-3 pb-4">
                <div class="text-center p-1 border-bottom">
                    <span class="float-left ${o ? "" : "text-muted"}">#${i + 1}</span>
                    Table ${loopOrders[i].tableNumber}
                </div>
                ${loopItems(loopOrders[i].items, i)}
                <div class="mt-5">
                    ${switchHistory ? `<span id="timer${i}">${formatTime(loopOrders[i].orderTime, 0, 100)}</span>`
                        : `<span class="text-muted" id="timer${i}">${formatTime(time, 14, 5)}</span>`}
                    <button class="btn btn-primary float-right" onclick="${switchHistory ? "retrieve" : "done"}Order(${i})">${switchHistory ? "Retrieve" : "Done"}</button>
                </div>
            </div>
        </div>
        `;
    }

    $("#orders-container").html(html);
}

function loopItems(items, index)
{
    let html = "";
    for (let i = 0; i < items.length; i++)
    {
        html += `
        <div>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="doneItem${index}-${i}">
                <label class="custom-control-label" for="doneItem${index}-${i}">
                    ${items[i].name} x ${items[i].count}
                    ${loopOptions(items[i].items, index, i)}
                </label>
            </div>
        </div>
        `;
    }
    return html;
}

function loopOptions(options, index1, index2)
{
    if (options)
    {
        let html = "";
        for (let i = 0; i < options.length; i++)
        {
            html += `
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="doneItem${index1}-${index2}-${i}">
                    <label class="custom-control-label" for="doneItem${index1}-${index2}-${i}">
                        ${options[i].name}
                    </label>
                </div>
            `;
        }
        return html;
    }
    return "";
}

async function doneOrder(index)
{
    const res = await (await fetch("/rest/doneOrder/" + orders[index].id, {method: "PUT"})).json();
    console.log(res);
}

async function retrieveOrder(index)
{
    const res = await (await fetch("/rest/retrieveOrder/" + history[index].id, {method: "PUT"})).json();
    console.log(res);
}


$("#historySwitch").change(function() {
    switchHistory = this.checked;
    displayOrders();
});

$("#tableSwitch").change(function() {
    filterTable = this.checked;
    displayOrders();
});

$("#table-filter").change(function() {
    filterTableNum = this.value;
    displayOrders();
});

setInterval(() => {
    if (!switchHistory)
    {
        for (let i = 0; i < orders.length; i++)
        {
            if (filterTable && filterTableNum != orders[i].tableNumber)
            {
                continue;
            }
            $("#timer" + i).html(formatTime(new Date().getTime() - orders[i].orderTime, 14, 5));
        }
    }
}, 1000);
