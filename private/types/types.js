

let list = [];

function displayList(filter) {
    if (!list) {
        return;
    }
    if (typeof filter == "string") {
        filter = filter.toUpperCase();
    }
    let html = "";
    for (let i = 0; i < list.length; i++) {
        if (!filter || (filter && typeof list[i].name == "string" && list[i].name.toUpperCase().includes(filter))) {
            html += `
            <div class="d-flex flex-column flex-lg-row justify-content-lg-between align-items-lg-center border-bottom p-3">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck${i}" ${list[i].show ? "checked" : ""} onclick="showType(customCheck${i},${list[i].id})">
                    <label class="custom-control-label h5" for="customCheck${i}">${list[i].name}</label>
                </div>
                <div class="my-1">
                    <button class="btn btn-secondary" onclick="moveType(${list[i].id}, 'false')"><i class="fas fa-chevron-down"></i></button>
                    <button class="btn btn-secondary ml-2" onclick="moveType(${list[i].id}, 'true')"><i class="fas fa-chevron-up"></i></button>
                    <button class="btn btn-danger ml-2" onclick="deleteType(${list[i].id})">Delete</button>
                </div>
            </div>
            `;
        }
    }

    $("#list-container").html(html);
}

async function getTypeList() {
    const res = await fetch('/types', { method: "GET" });
    const obj = await res.json();
    console.log(obj);
    list = obj.list;
    displayList();
}

async function createType() {
    const res = await fetch('/types', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            typeName: $("#newTypeName")[0].value
        })
    });
    console.log(await res.json());
    getTypeList();
}

async function deleteType(id) {
    const res = await fetch('/types?typeId=' + id, {
        method: "DELETE"
    });
    console.log(await res.json());
    getTypeList();
}

async function moveType(id, up) {
    const res = await fetch('/types?typeId=' + id + "&moveUp=" + up, {
        method: "PUT"
    });
    console.log(await res.json());
    getTypeList();
}

async function showType(checkbox, id) {
    if (checkbox) {
        const res = await fetch('/types/' + id + '/show/' + checkbox.checked, {
            method: "PUT"
        });
        if (res.status == 200) {
            getTypeList();
        } else {
            alert("error");
        }
    }
}

$("#search-input").keyup(function () {
    displayList(this.value);
});

getTypeList();

