import * as path from 'path';

import * as express from "express";
import * as bodyParser from "body-parser";
import * as expressSession from 'express-session';

import * as passport from "passport";
import * as passportLocal from "passport-local";
import * as passportOauth2 from "passport-oauth2";


import { checkPassword } from "./hash";
import { isLoggedIn } from "./guards";

import * as http from 'http';
import * as socketIO from 'socket.io';

//google sign in
import fetch from "cross-fetch";
import * as dotenv from "dotenv";

dotenv.config();
const { GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET } = process.env;



const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// app.use(expressSession({
// 	secret: 'Tecky Academy teaches typescript',
// 	resave: true,
// 	saveUninitialized: true
// }));

const sessionMiddleware = expressSession({
  secret: 'Tecky Academy teaches typescript',
  resave: true,
  saveUninitialized: true
});

//http server and socketIO
const server = new http.Server(app);
const io = socketIO(server);

app.use(sessionMiddleware);

//socketIO session
io.use((socket, next) => {
  sessionMiddleware(socket.request, socket.request.res, next);
});

io.on('connection', function (socket) {

  socket.request.session.socketId = socket.id;
  socket.request.session.save();

  socket.on("disconnect", () => {
    socket.request.session.socketId = null;
    socket.request.session.save();
  })

  socket.on("join", (room: string) => {
    socket.join(room);
    restaurantService.updateRestaurantOrders(room);
    restaurantService.updateEmptyTables(room);
  });

});


// passport
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user: { id: number }, done) {
  done(null, user);
});

passport.deserializeUser(function (user: { id: number }, done) {
  done(null, user);
});

const LocalStrategy = passportLocal.Strategy;
passport.use(
  new LocalStrategy(async function (username: string, password: string, done) {
    const users = userService.getUsers();
    const user = users.find(user => user.username == username);
    if (!user || user.googleUser) {
      return done(null, false, { message: "Incorrect username!" });
    }
    const match = await checkPassword(password, user.password);
    if (match) {
      return done(null, { id: user.id });
    } else {
      return done(null, false, { message: "Incorrect password!" });
    }
  })
);

//Google SignIn: Add Oauth2 Strategy
const OAuth2Strategy = passportOauth2.Strategy;
passport.use(
  "google",
  new OAuth2Strategy(
    {
      authorizationURL: "https://accounts.google.com/o/oauth2/auth",
      tokenURL: "https://accounts.google.com/o/oauth2/token",
      clientID: GOOGLE_CLIENT_ID ? GOOGLE_CLIENT_ID : "",
      clientSecret: GOOGLE_CLIENT_SECRET ? GOOGLE_CLIENT_SECRET : "",
      callbackURL: "http://localhost:8080/users/google/callback"
    },
    async function (
      accessToken: string,
      refreshToken: string,
      profile: any,
      done: Function
    ) {
      const res = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
        method: "get",
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      });
      const result = await res.json();
      const users = userService.getUsers();
      let user = users.find(user => user.username == result.email);
      if (!user) {
        userService.register({
          id: 0,
          googleUser: true,
          restName: "no restaurant name",
          email: result.email,
          username: result.email,
          password: ""
        })
        user = users.find(user => user.username == result.email);
      }
      if (user && user.googleUser) {
        done(null, { accessToken, refreshToken, id: user.id });
      }
      else {
        done(new Error("Failed to login with google account"));
      }
    }
  )
);


import { UserService } from './services/UserService';
import { UserRouter } from './routers/UserRouter';
const userService = new UserService();
const userRouter = new UserRouter(userService);

import { TableRouter } from "./routers/TableRouter";
import { TableService } from "./services/TableService";
const tableService = new TableService();

import { RestaurantRouter } from "./routers/RestaurantRouter";
import { RestaurantService } from "./services/RestaurantService";
const restaurantService = new RestaurantService(io, tableService, userService);

import { DishRouter } from "./routers/DishRouter";
import { DishService } from "./services/DishService";
const dishService = new DishService();

import { TypeRouter } from "./routers/TypeRouter";
import { TypeService } from "./services/TypeService";
const typeService = new TypeService();

import { QRCodeRouter } from "./routers/QRCodeRouter";
import { QRCodeService } from "./services/QRCodeService";
const qrCodeService = new QRCodeService();

app.use("/users", userRouter.router());
app.use('/images', express.static(path.join(__dirname, 'storages/', 'images/')));
app.use("/rest", new RestaurantRouter(restaurantService, dishService, typeService).router());
app.use(express.static(path.join(__dirname, 'public')));

//public rounting
app.use("/login", (req, res) => res.sendFile(path.join(__dirname, "public/login/login.html")));

//Logged in
app.use(isLoggedIn);

app.use(express.static(path.join(__dirname, 'private')));
app.use("/dish", new DishRouter(dishService).router());
app.use("/types", new TypeRouter(typeService).router());
app.use("/tables", new TableRouter(tableService).router());
app.use("/qrcode", new QRCodeRouter(qrCodeService, tableService).router());

//private rounting
// app.use("/settings", (req, res) => res.sendFile(path.join(__dirname, "private/settings/settings.html")));
// app.use("/orders", (req, res) => res.sendFile(path.join(__dirname, "private/orders/orders.html")));
// app.use("/tables", (req, res) => res.sendFile(path.join(__dirname, "private/tables/tables.html")));
// app.use("/managetypes", (req, res) => res.sendFile(path.join(__dirname, "private/types/types.html")));
// app.use("/managedishes", (req, res) => res.sendFile(path.join(__dirname, "private/dish/manage.html")));
// app.use("/newdish", (req, res) => res.sendFile(path.join(__dirname, "private/dish/dish.html")));
// app.use((req, res) => res.redirect("/settings"));

// Start the server
const PORT = 8080;

server.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});