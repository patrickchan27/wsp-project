import * as nodemailer from "nodemailer";

const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: process.env.MAILER_EMAIL,
        pass: process.env.MAILER_PASS
    }
});

export function sendMail(email: string, subject: string, html: string)
{
    let mailOptions = {
        from: process.env.MAILER_EMAIL,
        to: email,
        subject,
        html
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        }
        // else {
        //   console.log('Email sent: ' + info.response);
        // }
    });
}