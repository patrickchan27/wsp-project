import { Request, Response, NextFunction } from "express";


export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    if (req.user) {
        next();
    } else {
        res.redirect("/login");
    }
}

export function loginFlow(req: Request, res: Response, next: NextFunction) {
    return (err: Error, user: any, info: { message: string }) => {
        if (err) {
            res.redirect("/login?error=" + err.message);
            // res.json({
            //     result: false,
            //     message: err.message
            // });
        } else if (info && info.message) {
            res.redirect("/login?error=" + info.message);
            // res.json({
            //     result: false,
            //     message: info.message
            // });
        } else {
            req.logIn(user, err => {
                if (err) {
                    res.redirect("/login?error=" + "Failed to Login");
                    // res.json({
                    //     result: false,
                    //     message: "Failed to login"
                    // });
                } else {
                    res.redirect("/settings/settings.html");
                    // res.json({
                    //     result: true,
                    //     message: "Successfully logged in"
                    // });
                }
            });
        }
    };
}
